<?php

use yii\db\Migration;

class m170719_072229_insert_into_settings extends Migration
{
    public function safeUp()
    {
    	$this->insert('settings', [
            'name' => 'partner_distance',
            'value' => '50',
        ]);
        $this->insert('settings', [
            'name' => 'distance_price',
            'value' => '150',
        ]);
        $this->insert('settings', [
            'name' => 'phone',
            'value' => '800-2345-6789',
        ]);
    }

    public function safeDown()
    {
        echo "m170719_072229_insert_into_settings cannot be reverted.\n";
		$this->delete('settings');
        //return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170719_072229_insert_into_settings cannot be reverted.\n";

        return false;
    }
    */
}
