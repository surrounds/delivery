<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sms`.
 */
class m170629_064614_create_sms_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sms', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->defaultValue(0),
            'send_id' => $this->string(),
            'text' => $this->string(),
            'created' => $this->integer(),
            'status' => $this->string()->notNull()->defaultValue('pending'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sms');
    }
}
