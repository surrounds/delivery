<?php

use yii\db\Migration;

class m170613_165700_orders_alter extends Migration
{
    public function up()
    {
		$this->addColumn("{{%orders}}", "phone_to", $this->string()->defaultValue(0)->notNull());
    }

    public function down()
    {
        //echo "m170613_165700_orders_alter cannot be reverted.\n";
		$this->dropColumn("{{%orders}}", "phone_to");
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
