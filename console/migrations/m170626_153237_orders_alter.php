<?php

use yii\db\Migration;

class m170626_153237_orders_alter extends Migration
{
    public function up()
    {
		$this->addColumn("{{%orders}}", "recipient_id", $this->integer()->notNull());
    }

    public function down()
    {
		$this->dropColumn("{{%orders}}", "recipient_id");
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
