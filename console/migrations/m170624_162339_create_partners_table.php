<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partners`.
 */
class m170624_162339_create_partners_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('partners', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'commision' => $this->integer()->defaultValue(100),
            'distance' => $this->integer()->defaultValue(50),
            'carname' => $this->string()->defaultValue('van'),
            'maxweight' => $this->integer()->defaultValue(1000),
            'maxsize' => $this->integer()->defaultValue(1),
            'current_lat' => $this->string()->defaultValue(0),
            'current_lon' => $this->string()->defaultValue(0),
            'start_point' => $this->string()->defaultValue(''),
            'end_point' => $this->string()->defaultValue(''),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-partners-user_id',
            'partners',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-partners-user_id',
            'partners',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('partners');
    }
}
