<?php

use yii\db\Migration;

class m170614_165700_orders_alter extends Migration
{
    public function up()
    {
				$this->addColumn("{{%orders}}", "created", $this->string()->defaultValue(0)->notNull());
				$this->addColumn("{{%orders}}", "completed", $this->string()->defaultValue(0)->notNull());
				$this->addColumn("{{%orders}}", "status", $this->string()->defaultValue('new')->notNull());
    }

    public function down()
    {
        echo "m170614_165700_orders_alter cannot be reverted.\n";
				$this->dropColumn("{{%orders}}", "created");
				$this->dropColumn("{{%orders}}", "completed");
				$this->dropColumn("{{%orders}}", "status");
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
