<?php

use yii\db\Migration;

class m170726_084317_insert_into_cities extends Migration
{
    public function safeUp()
    {
		$this->insert('cities', ['id' =>291074,'name' => 'Ras al-Khaimah',]);
		$this->insert('cities', ['id' =>291763,'name' => 'Kalba',]);
		$this->insert('cities', ['id' =>291775,'name' => 'Jumayra',]);
		$this->insert('cities', ['id' =>292223,'name' => 'Dubai',]);
		$this->insert('cities', ['id' =>292239,'name' => 'Dibba Al-Hisn',]);
		$this->insert('cities', ['id' =>292261,'name' => 'Dayrah',]);
		$this->insert('cities', ['id' =>292672,'name' => 'Sharjah',]);
		$this->insert('cities', ['id' =>292878,'name' => 'Fujairah',]);
		$this->insert('cities', ['id' =>292913,'name' => 'Al Ain',]);
		$this->insert('cities', ['id' =>292932,'name' => 'Ajman',]);
		$this->insert('cities', ['id' =>292953,'name' => 'Adh Dhayd',]);
		$this->insert('cities', ['id' =>292968,'name' => 'Abu Dhabi',]);
		$this->insert('cities', ['id' =>385024,'name' => 'As Satwah',]);
		$this->insert('cities', ['id' =>6691081,'name' => 'Hor Al Anz',]);
		$this->insert('cities', ['id' =>6691091,'name' => 'Al Karama',]);
		$this->insert('cities', ['id' =>6691195,'name' => 'Shargan',]);
		$this->insert('cities', ['id' =>8469612,'name' => 'Al Nahda',]);
		$this->insert('cities', ['id' =>8469656,'name' => 'Ras al Khor',]);
		$this->insert('cities', ['id' =>8469668,'name' => 'International City',]);
		$this->insert('cities', ['id' =>8469814,'name' => 'Al Sufouh',]);
		$this->insert('cities', ['id' =>8476510,'name' => 'Al Majaz 3',]);
    }

    public function safeDown()
    {
        echo "m170726_084317_insert_into_cities.\n";
        $this->delete('cities');
        //return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170726_084317_insert_into_cities cannot be reverted.\n";

        return false;
    }
    */
}
