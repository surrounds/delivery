<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_types`.
 */
class m170430_104504_create_order_types_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_types', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_types');
    }
}
