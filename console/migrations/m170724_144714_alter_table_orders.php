<?php

use yii\db\Migration;

class m170724_144714_alter_table_orders extends Migration
{
    public function safeUp()
    {
		$this->addColumn("{{%orders}}", "date", $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn("{{%orders}}", "date");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170724_144714_alter_table_orders cannot be reverted.\n";

        return false;
    }
    */
}
