<?php

use yii\db\Migration;

class m170906_152406_alter_table_orders extends Migration
{
    public function safeUp()
    {
		$this->addColumn("{{%orders}}", "from_app", $this->string());
		$this->addColumn("{{%orders}}", "to_app", $this->string());
		$this->addColumn("{{%orders}}", "seats", $this->integer());
		$this->addColumn("{{%orders}}", "ancost", $this->integer());
    }

    public function safeDown()
    {
		$this->dropColumn("{{%orders}}", "from_app", $this->string());
		$this->dropColumn("{{%orders}}", "to_app", $this->string());
		$this->dropColumn("{{%orders}}", "seats", $this->integer());
		$this->dropColumn("{{%orders}}", "ancost", $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170906_152406_alter_table_orders cannot be reverted.\n";

        return false;
    }
    */
}
