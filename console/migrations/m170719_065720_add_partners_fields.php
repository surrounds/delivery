<?php

use yii\db\Migration;

class m170719_065720_add_partners_fields extends Migration
{
    public function safeUp()
    {
		$this->addColumn("{{%partners}}", "license", $this->string()->notNull());
		$this->addColumn("{{%partners}}", "status", $this->integer()->defaultValue(0)->notNull());
    }

    public function safeDown()
    {
        echo "m170719_065720_add_partners_fields cannot be reverted.\n";
		$this->dropColumn("{{%partners}}", "license");
		$this->dropColumn("{{%partners}}", "status");
        //return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170719_065720_add_partners_fields cannot be reverted.\n";

        return false;
    }
    */
}
