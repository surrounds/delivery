<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        /*'cache' => [
            'class' => 'yii\caching\FileCache',
        ],*/
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            	'login' => 'site/login',
            	'signup' => 'site/signup',
            	'logout' => 'site/logout',
            	'about' => 'site/about',
            	'contacts' => 'site/contact',
            	'services' => 'site/services',
            	'news' => 'site/news',
            	'tracking/<id:\d+>' => 'orders/track',
            	'track_search' => 'orders/track_search',
            	'profile' => 'site/profile',
            	'become_partner' => 'site/become_partner',
            	'terms' => 'site/terms',
            	'partnership' => 'site/partnership',
            	
            ],
        ],
		    'user' => [
		        'class' => 'frontend\components\User', // extend User component
		    ],
		    'settings' => [
		        'class' => 'frontend\components\SettingsComponent', // extend User component
		    ],
    ],
];
