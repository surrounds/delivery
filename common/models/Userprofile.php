<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "userprofile".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $city_id
 * @property string $street
 * @property string $bulding
 * @property string $apartment
 */
class Userprofile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userprofile';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name', 'city_id', 'phone', 'street', 'bulding', 'apartment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'city_id' => Yii::t('app', 'City'),
            'street' => Yii::t('app', 'Street'),
            'bulding' => Yii::t('app', 'Bulding'),
            'apartment' => Yii::t('app', 'Apartment'),
						'phone' => Yii::t('app', 'Phone'),
        ];
    }
}
