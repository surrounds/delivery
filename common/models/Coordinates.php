<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "coordinates".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $lat
 * @property integer $lon
 * @property integer $type_id
 * @property string $name
 */
class Coordinates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coordinates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'address'], 'required'],
            [['user_id', 'type_id'], 'integer'],
            [['lat', 'lng'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
            [['name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lon'),
            'city_id' => Yii::t('app', 'City'),
            'address' => Yii::t('app', 'Adress'),
            'house' => Yii::t('app', 'House'),
            'type_id' => Yii::t('app', 'Type ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
}
