<?php

namespace common\models;
use frontend\models\SignupForm;
use common\components\BaseController;
use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $client_id
 * @property integer $vendor_id
 * @property integer $from_id
 * @property integer $to_id
 * @property string $phone
 * @property integer $city_id
 * @property string $address
 * @property string $house
 * @property string $comment
 * @property integer $price
 * @property integer $weight
 * @property integer $length
 * @property integer $width
 * @property integer $height
 * @property integer $cost
 * @property integer $type_id
 */
class Orders extends \yii\db\ActiveRecord
{
    public $address_type;
    public $address_from;
    public $address_to;
    public $first_name;
    public $last_name;
    public $first_name_to;
    public $last_name_to;
    public $email;
	public $from_lat;
	public $from_lon;
	public $to_lat;
	public $to_lon;
	
    
    public static function tableName()
    {
        return 'orders';
    }

    public function rules()
    {
        return [
			[['client_id', 'phone', 'phone_to','first_name','last_name','first_name_to','last_name_to','phone_to','address_from','address_to'], 'required'],
			[['client_id', 'recipient_id', 'vendor_id', 'from_id', 'to_id', 'price', 'weight', 'length', 'width', 'height', 'total_km', 'cost', 'type_id','created','completed','seats','ancost'], 'integer'],
			[['from_lat', 'from_lon','to_lat','to_lon'], 'number', 'numberPattern' => '/^\s*[-+]?[0-9]*[.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/'],
			[['consignment', 'phone', 'phone_to', 'first_name', 'last_name', 'comment', 'address_type', 'address_from', 'date', 'address_to', 'from_app'], 'string', 'max' => 255],
			[['email'],'email'],
        ];
    }
    
    public function beforeSave($insert)
	{
	    if (parent::beforeSave($insert)) { 
	 
	        $this->consignment = '350000';
	        if($this->id)
	        	$this->consignment .= $this->id;
	        else{
	        	$maxId = Orders::find()->select('id')->orderBy(['id'=>'DESC'])->one();
	        	$this->consignment .= $maxId ? $this->id : 1;
	        }
	        	
	        $this->created = time();
	        
	        if($this->date)
	        	$this->date = strtotime($this->date);
	        	
	        if(!$this->price) $this->price = 0;	
	        $this->calculateCost();
	        
	        if(!$this->from_lat || !$this->from_lon || !$this->to_lat || !$this->to_lon)
	        	$this->setLatLng();
	        //echo $this->cost;exit;
	        	
		    if(Yii::$app->user->id == NULL){	    	
		    	$user = new User();
		    	$userprofile = new Userprofile();
	        
				$user->username = $this->findFreeUsername(mb_strtolower($this->last_name));
				if($this->email)
					$user->email = $this->email;
				else
					$user->email = $user->username.'@thisdom.com';
		        
		        $password = rand(100000,999999);
		        $user->setPassword($password);
		        $user->generateAuthKey();
		        $userprofile->first_name = $this->first_name;
		        $userprofile->last_name = $this->last_name;
		        $addr_arr = explode(",",$this->address_from);
		        if(count($addr_arr) > 1)
		        {
		        	$userprofile->street = $addr_arr[0];
		        	$userprofile->bulding = $addr_arr[1];
		    	}
		    	else
		    	{
		        	$userprofile->street = $this->address_from;
		    	}
		    	
		    	if($user->save()){
			    	/*
			    		 Add email sending user data
			    	*/
			    	$mail_text = "Your account data: Login " . $user->username . ", Password " . $password;
			    	$this->client_id = $user->id;
			    	$userprofile->user_id = $user->id;
			    	$userprofile->save();
		    	}
		    	//echo "<pre>";print_r($user);exit;
		    }
		    else{
		    	$this->client_id = Yii::$app->user->id;
		    	$userprofile = Userprofile::find()->where(['user_id' => Yii::$app->user->id])->one();
		    	
		    	if($userprofile->first_name != $this->first_name)
		    		$userprofile->first_name = $this->first_name;
		    	if($userprofile->last_name != $this->last_name)
		    		$userprofile->last_name = $this->last_name;
		    	if($userprofile->phone != $this->phone)
		    		$userprofile->phone = $this->phone;
		    	$userprofile->save($userprofile);
		    }
		    	
			if(!$userprofile = Userprofile::find()->where(['phone' => '%'.$this->phone_to.'%'])->one() && $this->first_name_to && $this->last_name_to){
				$user = new User();
		    	$userprofile = new Userprofile();
				$user->username = $this->findFreeUsername(mb_strtolower($this->last_name_to));
		        $user->email = $user->username.'@thisdom.com';
		        $password = rand(100000,999999);
		        $user->setPassword($password);
		        $user->generateAuthKey();
		        $userprofile->first_name = $this->first_name_to;
		        $userprofile->last_name = $this->last_name_to;
		        $addr_arr = explode(",",$this->address_to);
		        $userprofile->street = isset($addr_arr[0]) ? (string)$addr_arr[0] : "";
		        $userprofile->bulding = isset($addr_arr[1]) ? (string)$addr_arr[1] : "";
			    	
		    	if($user->save()){
			    	/*
			    		 Add email sending user data
			    	*/
			    	//$mail_text = "Your account data: Login " . $user->username . ", Password " . $password;
			    	$this->recipient_id = $user->id;
			    	$userprofile->user_id = $user->id;
			    	$userprofile->save();
		    	}
			}
			else
				$this->recipient_id = $userprofile->user_id;
				
		    $findCoord = false;
	    	if($this->address_type == 'add'){
				if($this->address_from && !$findCoord = Coordinates::find()->where(["name" => (string)$this->address_from])->one()){
					$coordinates = new Coordinates();
					$coordinates->user_id = Yii::$app->user->id ? Yii::$app->user->id : $this->client_id;
					$coordinates->address = (string)$this->address_from;
					$coordinates->name = (string)$this->address_from;
					$coordinates->lat = isset($this->from_lat) ? $this->from_lat : 0;
					$coordinates->lng = isset($this->from_lon) ? $this->from_lon : 0;
					$this->from_id = $coordinates->save() ? $coordinates->id : 0;
				}
				else
				{
					//if(!$this->from_id){
						$this->from_id = $findCoord ? $findCoord->id : 0;
					//}
				}
			    
				if($this->address_to && !$findCoord = Coordinates::find()->where(["name" => (string)$this->address_to])->one())
				{
					$coordinates = new Coordinates();
					$coordinates->user_id = Yii::$app->user->id ? Yii::$app->user->id : $this->client_id;
					$coordinates->address = (string)$this->address_to;
					$coordinates->name = (string)$this->address_to;
					$coordinates->lat = isset($this->to_lat) ? $this->to_lat : 0;
					$coordinates->lng = isset($this->to_lon) ? $this->to_lon : 0;
					$this->to_id = $coordinates->save() ? $coordinates->id : 0;
				}
				else{
					if(!$this->to_id){
						$this->to_id = $findCoord ? $findCoord->id : 0;
					}
				}
			}				    
			return parent::beforeSave($insert);
	    }
	    return false;
	}
	
	public function setLatLng()
	{
		if (!$this->from_lat && !$this->from_lon)
		{
			$geoloc = $this->sendMapsRequest($this->address_from);
			if($geoloc->status == "OK")
			{			
				$this->from_lat = $geoloc->results[0]->geometry->location->lat;
				$this->from_lon = $geoloc->results[0]->geometry->location->lng;
			}
		}
		if (!$this->to_lat && !$this->to_lon)
		{
			$geoloc = $this->sendMapsRequest($this->address_to);
			if($geoloc->status == "OK")
			{			
				$this->to_lat = $geoloc->results[0]->geometry->location->lat;
				$this->to_lon = $geoloc->results[0]->geometry->location->lng;
			}
		}
		if($this->from_lat && $this->from_lon && $this->to_lat && $this->to_lon)
			$this->getDirection();
	}
	
	private function getDirection()
	{
		$url = 'https://maps.googleapis.com/maps/api/directions/json?origin='.$this->from_lat.','.$this->from_lon.'&destination='.$this->to_lat.','.$this->to_lon.'&language=en&key==AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0';
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		$direction = json_decode(curl_exec($ch));
		//var_dump($direction->routes[0]->legs[0]->distance->value);exit;
		if(isset($direction->routes) && isset($direction->routes[0]))
			$this->total_km = $direction->routes[0]->legs[0]->distance->value;
	}
	
	private function sendMapsRequest($address)
	{
		$urlToCheck = 'https://maps.googleapis.com/maps/api/geocode/json?address='.str_replace(" ","+",$address).'&language=en&key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0';
		if(empty($ch))
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		}
		curl_setopt($ch, CURLOPT_URL, $urlToCheck);
		
		return json_decode(curl_exec($ch));
	}
	
	public function generateTTN()
	{
	  $client = $this->client_id ? $this->client_id : 0;
	  $recipient = $this->recipient_id ? $this->recipient_id : 0;
	  
	  $this->consignment = '350000' . (string)$this->id;
	}
	
	public function calculateCost()
	{		
        if(!$this->total_km)	$this->total_km = 0;
        if(!$this->weight)		$this->weight = 1;	
        if(!$this->length)		$this->length = 10;	
        if(!$this->width)		$this->width = 10;	
        if(!$this->height)		$this->height = 10;	
        
        $volumeWeight = ($this->width * $this->height * $this->length) / 4000;
        
        if($volumeWeight > $this->weight) $this->weight = $volumeWeight;
        
        $baseprice = ($this->total_km < 150000) ? 16 : 18;
        $baseAddition = ($this->total_km < 150000) ? 1.2 : 1.4;
        $addWeight = ($this->weight - 5 < 0) ? 0 : (($this->weight - 5) * $baseAddition);
        $payerPrice = ($this->type_id == 2) ? 10 : 0;
        
        $this->cost = $baseprice + $addWeight + $payerPrice;
        //if(!$this->price || $this->price == 0)
        //	$this->price = $this->cost;
	}
    /*
    public function save($runValidation = true, $attributeNames = NULL)
    {
    	
      $this->consignment = 45;
	    if(Yii::$app->user->id == NULL){	    	
	    	$user = new User();
	    	$userprofile = new Userprofile();
        $user->username = $this->findFreeUsername(mb_strtolower($this->last_name));
        $user->email = $this->email;
        $password = rand(100000,999999);
        $user->setPassword($password);
        $user->generateAuthKey();
        $userprofile->first_name = $this->first_name;
        $userprofile->last_name = $this->last_name;
        $addr_arr = explode(",",$this->address_from);
        $userprofile->street = $addr_arr[0];
        $userprofile->bulding = $addr_arr[1];
	    	
	    	if($user->save()){
		    	
		    		 //Add email sending user data
		    	
		    	$mail_text = "Your account data: Login " . $user->username . ", Password " . $password;
		    	$this->client_id = $user->id;
		    	$userprofile->user_id = $user->id;
		    	$userprofile->save();
	    	}
	    	//echo "<pre>";print_r($user);exit;
	    }
	    
    	if($this->address_type == 'add'){
	      if($this->address_from){
	      	$coordinates = new Coordinates();
	      	$coordinates->user_id = Yii::$app->user->id ? Yii::$app->user->id : $this->client_id;
	      	$coordinates->address = (string)$this->address_from;
	      	$coordinates->name = (string)$this->address_from;
	      	$this->from_id = $coordinates->save() ? $coordinates->id : 0;
	      }
	      
	      if($this->address_to){
	      	$coordinates = new Coordinates();
	      	$coordinates->user_id = Yii::$app->user->id ? Yii::$app->user->id : $this->client_id;
	      	$coordinates->address = (string)$this->address_to;
	      	$coordinates->name = (string)$this->address_to;
	      	$this->to_id = $coordinates->save() ? $coordinates->id : 0;
	      }
	    }
	    
	    return ($this->validate() && parent::save()) ? $this : false;
    }
    */
    private function findFreeUsername($username)
    {
      if(!User::find()->where(['username' => $username])->one()){
      	return $username;
      }
      else{
      	for($i = 1; $i<100; $i++){
      		$nusername = $username . $i;
      		if(!User::find()->where(['username' => $nusername])->one()){
		      	return $nusername;
		      }
      	}
      }
      return $username;
    }
	
	public function getOrdertype()
	{
		return $this->hasOne(OrderTypes::className(), ['id' => 'type_id']);
	}
	
	public function getClientfio()
	{
		return $this->hasOne(Userprofile::className(), ['user_id' => 'client_id']);
	}
	
	public function getRecipientfio()
	{
		return $this->hasOne(Userprofile::className(), ['user_id' => 'recipient_id']);
	}
	
	public function getVendorfio()
	{
		return $this->hasOne(Userprofile::className(), ['user_id' => 'vendor_id']);
	}
	
	public function getAddressfrom()
	{
		return $this->hasOne(Coordinates::className(), ['id' => 'from_id']);
	}

	public function getAddressto()
	{
		return $this->hasOne(Coordinates::className(), ['id' => 'to_id']);
	}
	
	public function getCountcost()
	{
		$distance = \common\components\BaseController::pointDistance(24.488413,54.630539,24.375689,54.731529);
	  return $distance;
	}
    
    public function getMindf()
    {
		$partner_id = Yii::$app->user->id ? Yii::$app->user->id : 0;
		$partnerPoints = PartnersPoints::find()->where(['partner_id' => $partner_id])->all();
		
		$minDist = NULL;
		if($partnerPoints)
		{
			foreach ($partnerPoints as $point)
			{
				$from_data = Coordinates::findOne($this->from_id);
  				$minDistCheck = ($from_data) ? BaseController::pointDistance($from_data->lat,$from_data->lng,$point->lat,$point->lng) : 0;
				
				if(!$minDist || $minDist > $minDistCheck)
					$minDist = $minDistCheck;
			}
		}
		return $minDist;
    }
    
    public function getMindt()
    {
		$partner_id = Yii::$app->user->id ? Yii::$app->user->id : 0;
		$partnerPoints = PartnersPoints::find()->where(['partner_id' => $partner_id])->all();
		
		$minDist = NULL;
		if($partnerPoints)
		{
			foreach ($partnerPoints as $point)
			{
				$to_data = Coordinates::findOne($this->to_id);
				$minDistCheck = ($to_data) ? BaseController::pointDistance($to_data->lat,$to_data->lng,$point->lat,$point->lng) : 0;
				
				if(!$minDist || $minDist > $minDistCheck)
					$minDist = $minDistCheck;
			}
		}
		return $minDist;
    }

    public function attributeLabels()
    {
        return [
			'id' => Yii::t('app', 'ID'),
			'client_id' => Yii::t('app', 'Sender'),
			'vendor_id' => Yii::t('app', 'Vendor'),
			'recipient_id' => Yii::t('app', 'Recipient'),
					
			'first_name' => Yii::t('app', 'Sender first name'),
			'last_name' => Yii::t('app', 'Sender last name'),
			'from_id' => Yii::t('app', 'Sender address'),
			'address_from' => Yii::t('app', 'Sender address'),
			'phone' => Yii::t('app', 'Sender phone'),
					
			'first_name_to' => Yii::t('app', 'Recipient first name'),
			'last_name_to' => Yii::t('app', 'Recipient last name'),
			'to_id' => Yii::t('app', 'Recipient address'),
			'address_to' => Yii::t('app', 'Recipient address'),
			'phone_to' => Yii::t('app', 'Recipient phone'),
					
			'comment' => Yii::t('app', 'Comment'),
			'price' => Yii::t('app', 'Cash on delivery'),
			'weight' => Yii::t('app', 'Weight'),
			'length' => Yii::t('app', 'Length'),
			'width' => Yii::t('app', 'Width'),
			'height' => Yii::t('app', 'Height'),
			'cost' => Yii::t('app', 'Cost'),
			'type_id' => Yii::t('app', 'Payer'),
			'created' => Yii::t('app', 'Created'),
			'completed' => Yii::t('app', 'Completed'),
			'date' => Yii::t('app', 'Send Date'),
			'seats' => Yii::t('app', 'Number of seats'),
			'ancost' => Yii::t('app', 'Announcement the cost'),
			'from_app' => Yii::t('app', 'App.'),
			'to_app' => Yii::t('app', 'App.'),
        ];
    }
}
