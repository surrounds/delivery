<?php

namespace common\models;
use SoapClient;
use Yii;

/**
 * This is the model class for table "sms".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property integer $created
 * @property string $status
 */
class Sms extends \yii\db\ActiveRecord
{
	private $soap;
	private $balance;
	
    public static function tableName()
    {
        return 'sms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created'], 'integer'],
            [['text', 'send_id', 'status'], 'string', 'max' => 255],
        ];
    }
    
    public function send($text,$number = '+380638191702')
    {
		$this->getSoap();
		
		$this->user_id = Yii::$app->user->id ? Yii::$app->user->id : 0;
		$this->text = $text;
		$this->created = time();
    	
    	$sms = [   
	        'sender' => 'PattonPPua',   
	        'destination' => $number,   
	        'text' => $text   
	    ];
	    
	    $result = $this->soap->SendSMS($sms);  
	    $this->send_id = $result->SendSMSResult->ResultArray[1];
	    $this->save();
    }
    
    public function getStatus($send_id = "bfc57082-5955-77c8-93e2-26da84128f9a")
    {
    	if(!$this->send_id)
    		return;
    		
    	$this->getSoap();
		$sms = ['MessageId' => $this->send_id];  
		$status = $this->soap->GetMessageStatus($sms);   
		
		if($status->GetMessageStatusResult == 'Сообщение доставлено получателю')
		{
			$this->status = 'done';
			$this->save();
		}
    }
    
    private function getSoap()
    {
		if($this->soap)
			return;
		
		$this->soap = new SoapClient('http://turbosms.in.ua/api/wsdl.html');
		
		$auth = [   
	        'login' => 'surrounds',   
	        'password' => '19111991'   
	    ];  
	    $result = $this->soap->Auth($auth);   
	    
    	$balance = $this->soap->GetCreditBalance();   
    	$this->balance = $balance->GetCreditBalanceResult;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'send_id' => 'Send Id',
            'text' => 'Text',
            'created' => 'Created',
            'status' => 'Status',
        ];
    }
}
