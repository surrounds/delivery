<?php

namespace frontend\controllers;

use Yii;
use common\models\Cities;
use common\models\Orders;
use common\models\OrdersSearch;
use common\models\Coordinates;
use common\models\Userprofile;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
Yii::$app->view->registerJsFile('/js/core.js', ['depends' => 'yii\web\JqueryAsset']);
/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
	public function beforeAction($action)
	{            
	    if ($action->id == 'checkphone' && $action->id == 'getlocaddr') {
	        $this->enableCsrfValidation = false;
	    }
	
	    return parent::beforeAction($action);
	}
    public function behaviors()
    {
        return [
			/*'access' => [
				'class' => AccessControl::className(),
				'only' => ['index','create','view','track','update','delate'],
				'rules' => [
					[
						'actions' => ['index','create','view','track','update','delate'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],*/
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $searchModel->client_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = Orders::find()->where(['client_id' => Yii::$app->user->id])->all();

        return $this->render('index', [
            //'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
			'model' => $model,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$model = $this->findModel($id);
    	if($model->consignment == '350000')
    		$model->consignment = '350000'.$model->id;
        return $this->render('view', [
            'model' => $model,
        ]);
    }
    
    public function actionTrack($id)
    {
    	if(strpos($id,'350000') !== FALSE)
    		$id = (int)substr($id,5,strlen($id));
    	$model = $this->findModel($id);
    	
        return $this->render('track', [
            'model' => $model,
        ]);
    }
    
    public function actionTrack_search($id)
    {
    	if(strpos($id,'35000') !== FALSE)
    		$id = (int)substr($id,5,strlen($id));
    	$model = OrdersSearch::findOne($id);
    	if(!$model)
    		return $this->redirect(['/']);
        return $this->render('track', [
            'model' => $model,
        ]);
    }
    
    public function actionGetlocaddr()
    {
		$address = Yii::$app->request->post('address');
		$address .= ' united arab emirates';
		$address = str_replace(" ","+",$address);
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&language=en&key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0';
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, $url);
		
		echo curl_exec($ch);
		
		//$direction = json_decode(curl_exec($ch));
    }
    
    public function actionCalculator()
    {
    	$val = NULL;

    	if(Yii::$app->request->isPost)
    	{
    		$order = new Orders;
    		$order->width = Yii::$app->request->post("width");
    		$order->height = Yii::$app->request->post("height");
    		$order->weight = Yii::$app->request->post("weight");
    		$order->length = Yii::$app->request->post("length");
    		$order->calculateCost();
    		$val = $order->cost;
    	}
    	if($val)
	    	$val = 'Delivery cost is '.$val;
	    else
	    	$val = 'Set your data for calculate';
		
		return $this->render('calculator',['val' => $val]);
    }
    
    public function actionCheckphone()
    {
    	$userArr = [];
		if((Yii::$app->request->post('phone')))
		{
			$users = Userprofile::find()->where(['like', 'phone', Yii::$app->request->post('phone')])->limit(3)->all();
			
			if($users)
			{
				foreach ($users as $user)
				{
					$userInfo = [];			
					$userInfo['id'] = $user->id;
					$userInfo['first_name'] = $user->first_name ? $user->first_name : "";
					$userInfo['last_name'] = $user->last_name ? $user->last_name : "";
					$userInfo['phone'] = $user->phone ? $user->phone : "";
					$userInfo['address'] = $user->street ? $user->street . ',' : "";
					$userInfo['address'] .= $user->bulding ? $user->bulding . ',': "";
					$userInfo['address'] .= $user->city_id ? Cities::findOne($user->city_id)->name : "";
					$userArr[$user->id] = $userInfo;
				}
			}
		}
		//\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return json_encode($userArr);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();
			
		if(!Yii::$app->user->isGuest)
		{
			$model->first_name = Yii::$app->user->attr('first_name');
			$model->last_name = Yii::$app->user->attr('last_name');
			$model->phone = Yii::$app->user->attr('phone');
		}
				
        if ($model->load(Yii::$app->request->post()) && $model->validate()) { //var_dump($model);exit;
    		$model->save();
    		$model->generateTTN();
    		$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id); 
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
        		$model->save();
        		$model->generateTTN();
        		$model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
        		if($model->client_id){
					if($userprofile = Userprofile::find()->where(['user_id' => $model->client_id])->one())
					{
        				$model->first_name = $userprofile->first_name ? $userprofile->first_name : '';
        				$model->last_name = $userprofile->last_name ? $userprofile->last_name : '';
        			}
        		}
        		if($model->date)
        			$model->date = date('Y-m-d',$model->date);
        		if($model->recipient_id && (int)$model->recipient_id > 0){
					if($userprofile = Userprofile::find()->where(['user_id' => $model->recipient_id])->one())
					{
        				$model->first_name_to = $userprofile->first_name ? $userprofile->first_name : '';
        				$model->last_name_to = $userprofile->last_name ? $userprofile->last_name : '';
        			}
        		}
        		if($model->from_id)
        			$model->address_from = Coordinates::findOne($model->from_id)->address;
        		if($model->to_id)
        			$model->address_to = Coordinates::findOne($model->to_id)->address;
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
