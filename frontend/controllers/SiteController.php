<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use common\models\Userprofile;
use common\models\Partners;
use common\models\Settings;
use common\models\PartnersPoints;

Yii::$app->view->registerJsFile('/js/core.js', ['depends' => 'yii\web\JqueryAsset']);
/**
 * Site controller
 */
class SiteController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout', 'signup'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					/*'logout' => ['post'],*/
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * Displays homepage.
	 *
	 * @return mixed
	 */
	public function actionIndex()
	{
		return $this->render('index');
	}
	
	public function actionServices()
	{
		return $this->render('services');
	}
	
	public function actionNews()
	{
		return $this->render('news');
	}
	
	public function actionTerms()
	{
		return $this->render('terms');
	}
	/**
	 * Logs in a user.
	 *
	 * @return mixed
	 */
	public function actionLogin()
	{
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Logs out the current user.
	 *
	 * @return mixed
	 */
	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

	/**
	 * Displays contact page.
	 *
	 * @return mixed
	 */
	public function actionContact()
	{
		$model = new ContactForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
				Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
			} else {
				Yii::$app->session->setFlash('error', 'There was an error sending your message.');
			}

			return $this->refresh();
		} else {
			return $this->render('contact', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Displays about page.
	 *
	 * @return mixed
	 */
	public function actionAbout()
	{
		return $this->render('about');
	}

	/**
	 * Signs user up.
	 *
	 * @return mixed
	 */
	public function actionSignup()
	{
		$model = new SignupForm();
		$profile = new Userprofile();
		if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {//$model->validate();var_dump($model->getErrors());exit;
			if ($user = $model->signup()) {
				if (Yii::$app->getUser()->login($user)) {
					$profile->user_id = $user->id;
					$profile->save();
					return $this->goHome();
				}
			}
		}

		return $this->render('signup', [
			'model' => $model,
			'profile' => $profile,
		]);
	}
	public function actionPartnership()
	{
		$model = new SignupForm();
		$profile = new Userprofile();
		$partners = new Partners();
		
		if ($model->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post()) && $partners->load(Yii::$app->request->post())) {//$model->validate();var_dump($model->getErrors());exit;
			if ($user = $model->signup()) {
				if (Yii::$app->getUser()->login($user)) {
					$profile->user_id = $user->id;
					$profile->save();
					$partners->user_id = $user->id;
					$partners->save();
					return $this->goHome();
					
					//return $this->redirect('/partners/profile');		
				}
			}
		}

		return $this->render('partnership', [
			'model' => $model,
			'profile' => $profile,
			'partners' => $partners,
		]);
	}
	
	public function actionProfile()
	{
		if(Yii::$app->user->isGuest)
			return $this->goHome();
			
		$profile = Userprofile::find()->where(['user_id' => Yii::$app->user->id])->one();
		if(!$profile)
			$profile = new Userprofile;
		$partners = Partners::find()->where(['user_id' => Yii::$app->user->id])->one();
		if(!$partners)
			$partners = new Partners;	
			
		$partnersPoints = $partners ? PartnersPoints::find()->select('lat,lng')->asArray()->where(['partner_id' => $partners->id])->all() : NULL;
		//var_dump(Yii::$app->user->id);exit;
		if(Yii::$app->request->isPost)
		{
			if($profile->load(Yii::$app->request->post()) && $partners->load(Yii::$app->request->post()))
			{
				if(!$partners->user_id)
					$partners->user_id = Yii::$app->user->id;
				if(!$profile->user_id)
					$profile->user_id = Yii::$app->user->id;
				//echo "<pre>";var_dump(Yii::$app->request->post('PartnersPoints'));exit;
				$profile->save();
				$partners->save();
			}
			if(count(Yii::$app->request->post('PartnersPoints')) > 0)
			{
				if($partnersPoints)
				{					
					$isSetPoints = [];
					foreach(Yii::$app->request->post('PartnersPoints') as $point)
					{
						$isSetPoints['lat'][] = $point['lat'];
						$isSetPoints['lng'][] = $point['lng'];
						if(!PartnersPoints::find()->where(['partner_id' => $partners->id,'lat'=>$point['lat'],'lng'=>$point['lng']])->one())
						{
							$partnersPoints = new PartnersPoints;
							$partnersPoints->partner_id = $partners->id;
							$partnersPoints->lat = $point['lat'];
							$partnersPoints->lng = $point['lng'];
							$partnersPoints->save();
						}
					}
					$partnerPointObj = PartnersPoints::find()->where(['NOT IN','lat',$isSetPoints['lat']])->andWhere(['NOT IN','lng',$isSetPoints['lng']])->all();
					//var_dump(PartnersPoints::find()->where(['NOT IN','lat',implode(',',$isSetPoints['lat'])])->where(['NOT IN','lng',implode(',',$isSetPoints['lng'])])->createCommand()->getRawSql());exit;
					//var_dump($partnerPointObj);exit;
					foreach ($partnerPointObj as $point)
					{
						$point->delete();
					}
				}
				else
				{
					foreach(Yii::$app->request->post('PartnersPoints') as $point)
					{
						$partnersPoints = new PartnersPoints;
						$partnersPoints->partner_id = $partners->id;
						$partnersPoints->lat = $point['lat'];
						$partnersPoints->lng = $point['lng'];
						$partnersPoints->save();
					}
				}
			}
			$partnersPoints = PartnersPoints::find()->select('lat,lng')->asArray()->where(['partner_id' => $partners->id])->all();
		
		}	
		if ($profile->load(Yii::$app->request->post())) {//$model->validate();var_dump($model->getErrors());exit;
			$profile->save();
		}
		return $this->render('profile',[
				'profile' => $profile,
				'partners' => $partners,
	        	'partnersPoints' => $partnersPoints,
			]);
	}
	
	public function actionBecome_partner()
	{
		$userprofile = Userprofile::find()->where(['user_id' => Yii::$app->user->id])->one();
		if(!$userprofile)
			$userprofile = new Userprofile;
		$partners = Partners::find()->where(['user_id' => Yii::$app->user->id])->one();
		if(!$partners)
			$partners = new Partners;			
			
		if(Yii::$app->request->isPost)
		{			
			if($userprofile->load(Yii::$app->request->post()) && $partners->load(Yii::$app->request->post()))
			{
				if(!$partners->user_id)
					$partners->user_id = Yii::$app->user->id;
				if(!$userprofile->user_id)
					$userprofile->user_id = Yii::$app->user->id;
				//echo "<pre>";var_dump(Yii::$app->request->post('PartnersPoints'));exit;
				$userprofile->save();
				$partners->save();
			}	
			return $this->redirect(Yii::$app->request->referrer);		
			/*
			return $this->render('become_partner',[
	        		'userprofile' => $userprofile,
	        		'partners' => $partners,
	        ]);*/
		}
		else
		{    			
	        return $this->render('become_partner',[
	        		'userprofile' => $userprofile,
	        		'partners' => $partners,
	        ]);
        }
	}

	/**
	 * Requests password reset.
	 *
	 * @return mixed
	 */
	public function actionRequestPasswordReset()
	{
		$model = new PasswordResetRequestForm();
		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			if ($model->sendEmail()) {
				Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

				return $this->goHome();
			} else {
				Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
			}
		}

		return $this->render('requestPasswordResetToken', [
			'model' => $model,
		]);
	}

	/**
	 * Resets password.
	 *
	 * @param string $token
	 * @return mixed
	 * @throws BadRequestHttpException
	 */
	public function actionResetPassword($token)
	{
		try {
			$model = new ResetPasswordForm($token);
		} catch (InvalidParamException $e) {
			throw new BadRequestHttpException($e->getMessage());
		}

		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
			Yii::$app->session->setFlash('success', 'New password saved.');

			return $this->goHome();
		}

		return $this->render('resetPassword', [
			'model' => $model,
		]);
	}
}
