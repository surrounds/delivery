<?php

namespace frontend\modules\partners\controllers;
use Yii;
use common\models\User;
use common\models\Userprofile;
use common\models\Partners;
use common\models\PartnersPoints;

class ProfileController extends \yii\web\Controller
{
    public function actionIndex()
    {
		$userprofile = Userprofile::find()->where(['user_id' => Yii::$app->user->id])->one();
		if(!$userprofile)
			$userprofile = new Userprofile;
		$partners = Partners::find()->where(['user_id' => Yii::$app->user->id])->one();
		if(!$partners)
			$partners = new Partners;
		
		$partnersPoints = $partners ? PartnersPoints::find()->select('lat,lng')->asArray()->where(['partner_id' => $partners->id])->all() : NULL;
		
		//var_dump(json_encode($partnersPoints));exit;			
		if(Yii::$app->request->isPost)
		{
			
			if($userprofile->load(Yii::$app->request->post()) && $partners->load(Yii::$app->request->post()))
			{
				if(!$partners->user_id)
					$partners->user_id = Yii::$app->user->id;
				if(!$userprofile->user_id)
					$userprofile->user_id = Yii::$app->user->id;
				//echo "<pre>";var_dump(Yii::$app->request->post('PartnersPoints'));exit;
				$userprofile->save();
				$partners->save();
			}
			//var_dump($partners);exit;
			if(count(Yii::$app->request->post('PartnersPoints')) > 0)
			{
				if($partnersPoints)
				{					
					$isSetPoints = [];
					foreach(Yii::$app->request->post('PartnersPoints') as $point)
					{
						$isSetPoints['lat'][] = $point['lat'];
						$isSetPoints['lng'][] = $point['lng'];
						if(!PartnersPoints::find()->where(['partner_id' => $partners->id,'lat'=>$point['lat'],'lng'=>$point['lng']])->one())
						{
							$partnersPoints = new PartnersPoints;
							$partnersPoints->partner_id = $partners->id;
							$partnersPoints->lat = $point['lat'];
							$partnersPoints->lng = $point['lng'];
							$partnersPoints->save();
						}
					}
					$partnerPointObj = PartnersPoints::find()->where(['NOT IN','lat',$isSetPoints['lat']])->andWhere(['NOT IN','lng',$isSetPoints['lng']])->all();
					//var_dump(PartnersPoints::find()->where(['NOT IN','lat',implode(',',$isSetPoints['lat'])])->where(['NOT IN','lng',implode(',',$isSetPoints['lng'])])->createCommand()->getRawSql());exit;
					//var_dump($partnerPointObj);exit;
					foreach ($partnerPointObj as $point)
					{
						$point->delete();
					}
				}
				else
				{
					foreach(Yii::$app->request->post('PartnersPoints') as $point)
					{
						$partnersPoints = new PartnersPoints;
						$partnersPoints->partner_id = $partners->id;
						$partnersPoints->lat = $point['lat'];
						$partnersPoints->lng = $point['lng'];
						$partnersPoints->save();
					}
				}
			}
			$partnersPoints = PartnersPoints::find()->select('lat,lng')->asArray()->where(['partner_id' => $partners->id])->all();
			return $this->render('index',[
	        		'userprofile' => $userprofile,
	        		'partners' => $partners,
	        		'partnersPoints' => $partnersPoints,
	        ]);
		}
		else
		{    			
	        return $this->render('index',[
	        		'userprofile' => $userprofile,
	        		'partners' => $partners,
	        		'partnersPoints' => $partnersPoints,
	        ]);
        }
    }

}
