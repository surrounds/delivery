<?php

namespace frontend\modules\partners;
use Yii;
Yii::$app->view->registerJsFile('/js/core.js', ['depends' => 'yii\web\JqueryAsset']);
/**
 * partners module definition class
 */
class Module extends \yii\base\Module
{
    public $defaultRoute = 'delivery';
    public $layout = 'admin';
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\partners\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
    /*
    		\Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'css' => ['css/bootstrap.css'],
            'js' => ['js/bootstrap.js'],
        ];*/
        parent::init();

        // custom initialization code goes here
    }
}
