<?php 

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
$this->title = 'Delivery';
?>

<div class="partners-default-index">
    <h3>Your current orders</h3>
    
    <section id="orders-index-section">
<div class="orders-index container">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
	    'rowOptions' => function ($model, $key, $index, $grid) {
	    		if($model->vendor_id == Yii::$app->user->id)
			    	return ['class' => 'partner-tr success'];
			    else
			    	return ['class' => 'partner-tr'];
			},
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            
            [
            	'class' => 'frontend\components\ButtonUpdateDelete',
        		'template'=>'{partner_view}',
        		'contentOptions' => ['style' => 'width: 70px;','class' => 'hidden visible-xs'],
				'headerOptions' => ['class' => 'hidden visible-xs'],
        		'header'=>'Actions', 
            ],
            //'id',
            [
				'attribute' => 'client_id',
				'value' => function($data){ return $data->clientfio['first_name'].' '.$data->clientfio['last_name'];},
				'contentOptions' => ['class' => 'hidden visible-md visible-lg'],
				'headerOptions' => ['class' => 'hidden visible-md visible-lg'],
				//'filter' => Html::activeDropDownList($searchModel, 'client_id', ArrayHelper::map(TypeData::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '   ']),
            ],
            [
				'attribute' => 'from_id',
				'value' => function($data){ return $data->addressfrom['name'];},
				//'filter' => Html::activeDropDownList($searchModel, 'from_id', ArrayHelper::map(TypeData::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '   ']),
            ], 
            [
				'attribute' => 'to_id',
				'value' => function($data){ return $data->addressto['name'];},
				//'filter' => Html::activeDropDownList($searchModel, 'to_id', ArrayHelper::map(TypeData::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '   ']),
            ], 
            [
            	'attribute' => 'price',
				'contentOptions' => ['class' => 'hidden visible-md visible-lg'],
				'headerOptions' => ['class' => 'hidden visible-md visible-lg'],
            ],
             'cost',
			[
				'label' => "Pickup distance",
				'format'=>"raw",
				'value' => function($data){return round($data->mindf)." km.";}
			],
			[
				'label' => "End point distance",
				'format'=>"raw",
				'value' => function($data){return round($data->mindt)." km.";}
			],
            // 'type_id',
            [
            	'class' => 'frontend\components\ButtonUpdateDelete',
        		'template'=>'{partner_view}',
        		'contentOptions' => ['style' => 'width: 100px;'],
        		'header'=>'Actions', 
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
</section>

</div>
