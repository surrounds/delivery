<!-- Services -->
        <section class="well-lg">
            <div class="container">
                <h2>News Center</h2>
                <div class="row offset-1">
                    <div class="col-md-6">
                        <img src="images/page-4_img01.jpg" alt="" width="570" height="439">
                    </div>
                    <div class="col-md-6">
                        <!-- News post -->
                        <div class="news-post text-md-left">
                            <h3><a href="#">Believe in the Business of Your Dreams</a></h3>
                            <time datetime="2016-10-25">25 october, 2016</time>
                            <p>You may be wondering if you have the necessary skills, time, connections, and a million other things in order to create the business of your dreams. If you let your uncertainty and insecurity overpower you, you won't ever be able to unleash your true business potential.</p>
                            <a href="#" class="btn btn-sm btn-primary">
                                <i class="icon fa-angle-right"></i>
                                <span>read more</span>
                            </a>
                        </div>
                        <!-- END News post -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <img src="images/page-4_img02.jpg" alt="" width="570" height="439">
                    </div>
                    <div class="col-md-6">
                        <!-- News post -->
                        <div class="news-post text-md-left">
                            <h3><a href="#">Impact - The Heart of Business</a></h3>
                            <time datetime="2016-10-25">25 october, 2016</time>
                            <p>One of the most successful entrepreneurs featured at the Forbes website, Wendy Lipton - Dibner said that "the success of your business would solely depend on you. The only thing you can rely on is your power to achieve your goal".</p>
                            <a href="#" class="btn btn-sm btn-primary">
                                <i class="icon fa-angle-right"></i>
                                <span>read more</span>
                            </a>
                        </div>
                        <!-- END News post -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <img src="images/page-4_img03.jpg" alt="" width="570" height="439">
                    </div>
                    <div class="col-md-6">
                        <!-- News post -->
                        <div class="news-post text-md-left">
                            <h3><a href="#">How to Keep Your Heart Healthy</a></h3>
                            <time datetime="2016-10-25">25 october, 2016</time>
                            <p>When we speak of heart, we cannot miss out on the importance and benefits of plant foods. Vegetables are an excellent source of glutamic acid. It is a class of amino acid which helps keeping blood pressure at lower levels; safe levels, so to say.</p>
                            <a href="#" class="btn btn-sm btn-primary">
                                <i class="icon fa-angle-right"></i>
                                <span>read more</span>
                            </a>
                        </div>
                        <!-- END News post -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <img src="images/page-4_img04.jpg" alt="" width="570" height="439">
                    </div>
                    <div class="col-md-6">
                        <!-- News post -->
                        <div class="news-post text-md-left">
                            <h3><a href="#">Proper Color Solutions for the Office</a></h3>
                            <time datetime="2016-10-25">25 october, 2016</time>
                            <p>When it comes to remodeling an office, one of the most important aspects is painting. Any shade of paint can change the complete look of a room as a color has an ability to change a drab and boring room into a stunning one.</p>
                            <a href="#" class="btn btn-sm btn-primary">
                                <i class="icon fa-angle-right"></i>
                                <span>read more</span>
                            </a>
                        </div>
                        <!-- END News post -->
                    </div>
                </div>
            </div>
        </section>
        <!-- END Services -->