<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
//use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Contact Form -->
        <section class="well-lg well-lg--inset-1">
            <div class="container">
                <h2>Contact Form</h2>
                <!-- RD Mailform -->
                <form class='rd-mailform' method="post" action="bat/rd-mailform.php">
                    <!-- RD Mailform Type -->
                    <input type="hidden" name="form-type" value="contact"/>
                    <!-- END RD Mailform Type -->
                    <fieldset>
                        <div class="row">
                            <div class="col-md-4">
                                <label data-add-placeholder>
                                    <input type="text"
                                           name="name"
                                           placeholder="Name"
                                           data-constraints="@NotEmpty @LettersOnly"/>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label data-add-placeholder>
                                    <input type="text"
                                           name="email"
                                           placeholder="E-mail"
                                           data-constraints="@NotEmpty @Email"/>
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label data-add-placeholder>
                                    <input type="text"
                                           name="phone"
                                           placeholder="Telephone"
                                           data-constraints="@Phone"/>
                                </label>
                            </div>
                        </div>
                        <label data-add-placeholder>
                            <textarea name="message" placeholder="Comment"
                                      data-constraints="@NotEmpty"></textarea>
                        </label>
                        <div class="mfControls btn-group text-center">
                            <button class="btn btn-sm btn-primary" type="submit">
                                <i class="icon fa-envelope-o"></i>
                                <span>Send</span>
                            </button>
                        </div>
                        <div class="mfInfo"></div>
                    </fieldset>
                </form>
                <!-- END RD Mailform -->
            </div>
        </section>
        <!-- END Contact Form -->

        <!-- Nearest Depot -->
        <section>
            <div class="well-xs triangle-top">
                <h2>Nearest Depot</h2>
            </div>
            <!-- RD Google Map -->
            <div class="rd-google-map">
                <div id="google-map" class="rd-google-map__model" data-zoom="10" data-x="-73.9874068"
                     data-y="40.643180"></div>
                <ul class="rd-google-map__locations">
                    <li data-x="-73.9874068" data-y="40.643180">
                        <p class="text-primary heading-6">Depot #24</p>
                        <p>9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span><?=Yii::$app->settings->phone?></span></p>
                        <p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span><?=Yii::$app->settings->phone?></a></p>
                        <p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
                    </li>
                    <li data-x="-73.9274068" data-y="40.683180">
                        <p class="text-primary heading-6">Depot #25</p>
                        <p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
                        <p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span><?=Yii::$app->settings->phone?></a></p>
                        <p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
                    </li>
                    <li data-x="-73.8274068" data-y="40.583180">
                        <p class="text-primary heading-6">Depot #26</p>
                        <p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
                        <p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span><?=Yii::$app->settings->phone?></a></p>
                        <p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
                    </li>
                    <li data-x="-74.0274068" data-y="40.883180">
                        <p class="text-primary heading-6">Depot #27</p>
                        <p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
                        <p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span><?=Yii::$app->settings->phone?></a></p>
                        <p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
                    </li>
                    <li data-x="-74.4274068" data-y="40.783180">
                        <p class="text-primary heading-6">Depot #28</p>
                        <p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
                        <p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span><?=Yii::$app->settings->phone?></a></p>
                        <p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
                    </li>
                    <li data-x="-73.5274068" data-y="40.783180">
                        <p class="text-primary heading-6">Depot #29</p>
                        <p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
                        <p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span><?=Yii::$app->settings->phone?></a></p>
                        <p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
                    </li>
                </ul>
            </div>
            <!-- END RD Google Map -->
        </section>
        <!-- END Nearest Depot-->

