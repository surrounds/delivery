<?php 

?>

<!-- Services -->
        <section class="well-lg well-lg--inset-1">
            <div class="container">
                <h2>Services</h2>
                <p>DeliveryCo.is America’s leading same-day transport and logistics services company. Our dedicated courier service includes point-to-point delivery, fleet outsourcing, facilities management and custom transportation solutions.</p>
            </div>
            <div class="row row-no-gutter">
                <div class="col-md-6 bg-img-5">
                    <!-- Event post -->
                    <div class="event-post">
                        <h2>Definite Time & Day</h2>
                        <p>We offer the widest range of definite time and day parcel delivery
                            services in the market.</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="icon fa-angle-right"></i>
                            <span>read more</span>
                        </a>
                        <div></div>
                    </div>
                    <!-- END Event post -->
                </div>
                <div class="col-md-6 bg-img-6">
                    <!-- Event post -->
                    <div class="event-post">
                        <h2>Same Day Delivery</h2>
                        <p>The Same Day collection and delivery services.</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="icon fa-angle-right"></i>
                            <span>read more</span>
                        </a>
                        <div></div>
                    </div>
                    <!-- END Event post -->
                </div>
                <div class="col-md-6 bg-img-7">
                    <!-- Event post -->
                    <div class="event-post">
                        <h2>Freight Services</h2>
                        <p>We offer a range of highly flexible and reliable express
                            freight services.</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="icon fa-angle-right"></i>
                            <span>read more</span>
                        </a>
                        <div></div>
                    </div>
                    <!-- END Event post -->
                </div>
                <div class="col-md-6 bg-img-8">
                    <!-- Event post -->
                    <div class="event-post">
                        <h2>Additional Services</h2>
                        <p>We make sure that our services meet the demands of your business, making shipping simple, easy and stress-free.</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="icon fa-angle-right"></i>
                            <span>read more</span>
                        </a>
                        <div></div>
                    </div>
                    <!-- END Event post -->
                </div>
                <div class="col-md-6 bg-img-9">
                    <!-- Event post -->
                    <div class="event-post">
                        <h2>Courier Services</h2>
                        <p>DeliveryCo. is able to offer a courier service to suit you and your business needs.</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="icon fa-angle-right"></i>
                            <span>read more</span>
                        </a>
                        <div></div>
                    </div>
                    <!-- END Event post -->
                </div>
                <div class="col-md-6 bg-img-10">
                    <!-- Event post -->
                    <div class="event-post">
                        <h2>Special Services</h2>
                        <p>Exceptional service for exceptional shipments.</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="icon fa-angle-right"></i>
                            <span>read more</span>
                        </a>
                        <div></div>
                    </div>
                    <!-- END Event post -->
                </div>
            </div>
        </section>
        <!-- END Services -->

        <!-- Our Prices -->
        <section class="well-lg bg-contrast-variant-1">
            <div class="container">
                <h2>Our Prices</h2>
                <div class="row flow-offset-1 offset-1">
                    <div class="col-sm-6 col-md-4">
                        <div class="pricing-box">
                            <h5 class="pricing-box__header">ParcelShop Drop-off</h5>
                            <div class="pricing-box__body">
                                <span>From</span>
                                <p class="price">$2.50</p>
                            </div>
                            <div class="pricing-box__footer">
                                <p>Between 0-1kg</p>
                                <p>Up to £25 compensation</p>
                                <p>Over 5,000 nationwide</p>
                                <a href="#" class="btn btn-sm btn-primary">
                                    <i class="icon fa-angle-right"></i>
                                    <span>full price list</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                        <div class="pricing-box">
                            <h5 class="pricing-box__header">Courier Collection</h5>
                            <div class="pricing-box__body">
                                <span>From</span>
                                <p class="price">$3.70</p>
                            </div>
                            <div class="pricing-box__footer">
                                <p>Between 0-1kg</p>
                                <p>Up to £25 compensation</p>
                                <p>Door to door courier service</p>
                                <a href="#" class="btn btn-sm btn-primary">
                                    <i class="icon fa-angle-right"></i>
                                    <span>full price list</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-sm-preffix-3 col-md-4 col-md-preffix-0">
                        <div class="pricing-box">
                            <h5 class="pricing-box__header">Compensation</h5>
                            <div class="pricing-box__body">
                                <span>From</span>
                                <p class="price">+$1.50</p>
                            </div>
                            <div class="pricing-box__footer">
                                <p>Between 0-1kg</p>
                                <p>Up to £25 compensation</p>
                                <p>Dedicated customer helpline</p>
                                <a href="#" class="btn btn-sm btn-primary">
                                    <i class="icon fa-angle-right"></i>
                                    <span>full price list</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Our Prices -->