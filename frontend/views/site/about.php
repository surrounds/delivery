<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Company Profile -->
        <section class="well-lg">
            <div class="container">
                <h2>Company Profile</h2>
                <img src="images/page-2_img01.jpg" alt="" width="1170" height="498">
                <p class="inset-2">Our Company has been operating successfully since 2004. All of our staff have many
                    years experience in the Delivery industry. Our aim is to provide an efficient, stress free, friendly
                    Service at competitive rates.</p>
                <p class="inset-2">Over the last 8 years we have built up a large Client base ranging from regular
                    office moves, delivering furniture for exclusive stores and also delivering bespoke kitchens
                    nationwide. We also have a large repeat
                    customer base, whether it be moving house again, having furniture delivered, furniture built or
                    moved around their office or home.</p>
            </div>
        </section>
        <!-- END Company Profile -->

        <section>
            <div class="row row-no-gutter">
                <div class="col-md-6 bg-img-3">
                    <!-- Event post -->
                    <div class="event-post">
                        <h2>Our Mission & Vision</h2>
                        <p>We make our customers, employees and investors <br class="hidden visible-lg-block"> more
                            successful.</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="icon fa-angle-right"></i>
                            <span>read more</span>
                        </a>
                        <div></div>
                    </div>
                    <!-- END Event post -->
                </div>
                <div class="col-md-6 bg-img-4">
                    <!-- Event post -->
                    <div class="event-post">
                        <h2>Awards</h2>
                        <p>View our recent company awards</p>
                        <a href="#" class="btn btn-sm btn-primary">
                            <i class="icon fa-angle-right"></i>
                            <span>read more</span>
                        </a>
                        <div></div>
                    </div>
                    <!-- END Event post -->
                </div>
            </div>
        </section>

        <!-- How it works... -->
        <section class="well-lg">
            <div class="container">
                <h2>How it works...</h2>
                <div class="row flow-offset-1 offset-1">
                    <div class="col-xs-6 col-sm-4 box">
                        <span class="icon icon-xl triangle"><span
                                class="icon icon-xl icon-primary thin-icon-layers"></span></span>
                        <p class="heading-5"><a href="#">Compare</a></p>
                        <p class="offset-2">Get instant quotes & compare from a range of courier delivery services.</p>
                    </div>
                    <div class="col-xs-6 col-sm-4 box">
                        <span class="icon icon-xl triangle"><span
                                class="icon icon-xl icon-primary thin-icon-cart-add"></span></span>
                        <p class="heading-5"><a href="#">Send</a></p>
                        <p class="offset-2">Our system makes booking your delivery a breeze</p>
                    </div>
                    <div class="col-xs-6 col-xs-preffix-3 col-sm-4 col-sm-preffix-0 box">
                        <span class="icon icon-xl triangle"><span
                                class="icon icon-xl icon-primary thin-icon-box"></span></span>
                        <p class="heading-5"><a href="#">Save</a></p>
                        <p class="offset-2">Save a bundle with our discounts and special offers.</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- END How it works... -->

        <!-- Our Team -->
        <section class="well-lg bg-contrast-variant-1">
            <div class="container">
                <h2>Our Team</h2>
                <div class="row flow-offset-1 offset-1">
                    <div class="col-xs-6 col-md-3">
                        <img src="images/page-2_img04.jpg" alt="" width="270" height="314">
                        <p class="heading-5">Sean Damon</p>
                        <p class="offset-4">Chief Operating Officer</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <img src="images/page-2_img05.jpg" alt="" width="270" height="314">
                        <p class="heading-5">Tom Gene</p>
                        <p class="offset-4">Company Founder</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <img src="images/page-2_img06.jpg" alt="" width="270" height="314">
                        <p class="heading-5">Denise Hope</p>
                        <p class="offset-4">Development Manager</p>
                    </div>
                    <div class="col-xs-6 col-md-3">
                        <img src="images/page-2_img07.jpg" alt="" width="270" height="314">
                        <p class="heading-5">Alan Smith</p>
                        <p class="offset-4">Data Analyst</p>
                    </div>
                </div>
            </div>
        </section>
        <!-- END Our Team -->
