<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<!-- Fast and reliable -->
		<section class="well-md">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-preffix-2">
						<h2>Fast and reliable delivery services that meet your timeline.</h2>
					</div>
				</div>
				<div class="row flow-offset-1 offset-3">
					<div class="col-xs-6 col-sm-3 box">
						<a href="/orders/calculator/">
							<span class="icon icon-xl triangle"><span class="icon icon-xl icon-primary thin-icon-calculator"></span></span>
							<p class="heading-5"><a href="#">Delivery Price</a></p>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 box">
						<a href="/terms">
							<span class="icon icon-xl triangle"><span class="icon icon-xl icon-primary thin-icon-timer"></span></span>
							<p class="heading-5"><a href="#">Delivery Term</a></p>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 box">
						<a href="/services">
							<span class="icon icon-xl triangle"><span class="icon icon-xl icon-primary thin-icon-flag"></span></span>
							<p class="heading-5"><a href="#">Nearest Depot</a></p>
						</a>
					</div>
					<div class="col-xs-6 col-sm-3 box">
						<a href="/orders/create/">
							<span class="icon icon-xl triangle"><span class="icon icon-xl icon-primary thin-icon-ambulance"></span></span>
							<p class="heading-5"><a href="#">Call for a Courier</a></p>
						</a>
					</div>
				</div>
			</div>
		</section>
		<!-- END Fast and reliable-->

		<!-- Track search -->
		<section class="well-sm bg-primary">
			<div class="container">
				<div class="row">
					<div class="col-sm-10 col-sm-preffix-1 col-md-8 col-md-preffix-2 col-lg-6 col-lg-preffix-3">
						<h2>Track search</h2>
						<form class="search-form" action="/track_search" method="get">
							<label><input type="text" name="id" placeholder="Enter the invoice number"/></label>
							<button class="btn btn-sm btn-default" type="submit">
								<i class="icon fa-angle-right"></i>
								<span>Search</span>
							</button>
						</form>
					</div>
				</div>
			</div>
		</section>
		<!-- END Track search-->

		<section>
			<div class="row row-no-gutter">
				<div class="col-md-6 bg-img-1">
					<!-- Event post -->
					<div class="event-post">
						<h2>For Private Customers</h2>
						<p>It’s totally pure and simple. All you have to do is to drop into the nearest DeliveryCo. depot or book a courier to your home or office.</p>
						<a href="#" class="btn btn-sm btn-primary">
							<i class="icon fa-angle-right"></i>
							<span>read more</span>
						</a>
					</div>
					<!-- END Event post -->
				</div>
				<div class="col-md-6 bg-img-2">
					<!-- Event post -->
					<div class="event-post">
						<h2>For Business Customers</h2>
						<p>Manage your time and expenses by selecting the option that suits you best. The service is available for shipments from all over the world to US and vice versa.</p>
						<a href="#" class="btn btn-sm btn-primary">
							<i class="icon fa-angle-right"></i>
							<span>read more</span>
						</a>
					</div>
					<!-- END Event post -->
				</div>
			</div>
		</section>

		<!-- Nearest Depot -->
		<!--section>
			<div class="well-xs triangle-top">
				<h2>Nearest Depot</h2>
			</div>
			<!-- RD Google Map -->
			<!--div class="rd-google-map">
				<div id="google-map" class="rd-google-map__model" data-zoom="10" data-x="-73.9874068"
					 data-y="40.643180"></div>
				<ul class="rd-google-map__locations">
					<li data-x="-73.9874068" data-y="40.643180">
						<p class="text-primary heading-6">Depot #24</p>
						<p>9870 St Vincent Place, Glasgow, DC 45 Fr 45. <span>800 2345-6789</span></p>
						<p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span>800 2345-6789</a></p>
						<p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
					</li>
					<li data-x="-73.9274068" data-y="40.683180">
						<p class="text-primary heading-6">Depot #25</p>
						<p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
						<p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span>800 2345-6789</a></p>
						<p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
					</li>
					<li data-x="-73.8274068" data-y="40.583180">
						<p class="text-primary heading-6">Depot #26</p>
						<p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
						<p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span>800 2345-6789</a></p>
						<p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
					</li>
					<li data-x="-74.0274068" data-y="40.883180">
						<p class="text-primary heading-6">Depot #27</p>
						<p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
						<p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span>800 2345-6789</a></p>
						<p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
					</li>
					<li data-x="-74.4274068" data-y="40.783180">
						<p class="text-primary heading-6">Depot #28</p>
						<p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
						<p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span>800 2345-6789</a></p>
						<p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
					</li>
					<li data-x="-73.5274068" data-y="40.783180">
						<p class="text-primary heading-6">Depot #29</p>
						<p>28 Jackson Blvd Ste 1020 Chicago IL 60604-2340</p>
						<p><a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span>800 2345-6789</a></p>
						<p class="left-icon"><span class="icon icon-xs icon-primary fa-clock-o"></span>7 Days a week from 9:00 am to 7:00 pm</p>
					</li>
				</ul>
			</div>
			<!-- END RD Google Map -->
		</section-->
		<!-- END Nearest Depot-->
