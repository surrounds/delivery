<?php 
namespace frontend\views;
use Yii;
use yii\widgets\Pjax;
use yii\helpers\Html;


?>
<div class="container">

<?php Pjax::begin(['id' => 'calc-pj','timeout' => 50000 ]); ?>
	<?= Html::beginForm(['orders/calculator'], 'post', ['data-pjax' => '', 'class' => 'form-horizontal col-md-4 col-md-offset-4']); ?>
	<div class="form-group field-userprofile-first_name required">
		<label class="control-label" for="userprofile-first_name">Width</label>
		<?= Html::input('text', 'width', Yii::$app->request->post('width'), ['class' => 'form-control']) ?>		
		<p class="help-block help-block-error"></p>
	</div>
	
	<?= Html::beginForm(['orders/calculator'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
	<div class="form-group field-userprofile-first_name required">
		<label class="control-label" for="userprofile-first_name">Height</label>
		<?= Html::input('text', 'height', Yii::$app->request->post('height'), ['class' => 'form-control']) ?>		
		<p class="help-block help-block-error"></p>
	</div>
	
	<?= Html::beginForm(['orders/calculator'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
	<div class="form-group field-userprofile-first_name required">
		<label class="control-label" for="userprofile-first_name">Weight</label>
		<?= Html::input('text', 'weight', Yii::$app->request->post('weight'), ['class' => 'form-control']) ?>		
		<p class="help-block help-block-error"></p>
	</div>
	
	<?= Html::beginForm(['orders/calculator'], 'post', ['data-pjax' => '', 'class' => 'form-inline']); ?>
	<div class="form-group field-userprofile-first_name required">
		<label class="control-label" for="userprofile-first_name">Length</label>
		<?= Html::input('text', 'length', Yii::$app->request->post('length'), ['class' => 'form-control']) ?>		
		<p class="help-block help-block-error"></p>
	</div>
		
		<?= Html::submitButton('Calculate', ['class' => 'btn btn-info', 'name' => 'hash-button']) ?>
	<?= Html::endForm() ?>
	<p></p>
	<p class="col-md-12"><?=$val?></p>
<?php Pjax::end(); ?>
</div>


