<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\OrderTypes;
use common\models\Cities;
use common\models\Coordinates;
use dosamigos\datepicker\DatePicker;


/* @var $this yii\web\View */
/* @var $model common\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>
		
<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php $userId = isset(Yii::$app->user->id) ? Yii::$app->user->id : '1'; ?>
    <?= $form->field($model, 'client_id')->hiddenInput(['value' => $userId])->label(false) ?>
    <?= $form->field($model, 'address_type')->hiddenInput(['value' => 'add'])->label(false)?>
    <?= $form->field($model, 'from_lat')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'from_lon')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'to_lat')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'to_lon')->hiddenInput()->label(false)?>
    <?= $form->field($model, 'total_km')->hiddenInput()->label(false)?>
    
    <div class="col-md-12">
    	<div class="col-md-6">
				<div class="col-md-12 name-inform">
					<div class="col-md-6">
						<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-6 with-left-padding">
						<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
					</div>						
				</div>	
				
				<div class="col-md-12">
					<div class="col-md-7">
						<?= $form->field($model, 'phone',['template' => '
							<div class="form-group field-orders-address_to add-my-place-block">
								{label}
								<div class="input-group">
									<div class="input-group-addon">
										<img src="/images/phone-icon.png" width="32" height="32" alt="phone-icon">
									</div>
									{input}			
								</div>		
								<div class="inform-block"></div>
								<div class="help-block"></div>
							</div>
							'])->textInput(['maxlength' => true]) ?>
					</div>
					
					<div class="col-md-5">
						<?= $form->field($model, 'date')->widget(
				    		DatePicker::className(), [				    		
				    		'template' => '<div class="input-group-addon"><img src="/images/calendar-icon.png" width="32" height="32" alt="phone-icon"></div>{input}',
							    'clientOptions' => [
							            'autoclose' => true,
							            'format' => 'dd-mm-yyyy'
				        		]]); ?>
					</div>
				</div>
				<div class="col-md-12">
					<div class="col-md-5 add-my-place-block">
						<?= $form->field($model, 'address_from')->textInput(['maxlength' => true]) ?>
					</div>
					
					<div class="col-md-2">
						<?= $form->field($model, 'from_app')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-5 form-group find-btn-div">
						<label class="control-label button-location-order">&nbsp</label>
						<button type="button" class="find-me btn btn-success btn-block">Find My Location</button>
					</div>
				</div>
				<div id="map"></div>
				
    	</div>
    	
    	<div class="col-md-6">
				<div class="col-md-12 name-inform">
					<div class="col-md-6">
						<?= $form->field($model, 'first_name_to')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-6 with-left-padding">
						<?= $form->field($model, 'last_name_to')->textInput(['maxlength' => true]) ?>
					</div>	
				</div>	
				
				<div class="col-md-12">
					<div class="col-md-12 form-group field-orders-phone_to required">
						<label class="control-label" for="orders-phone_to">Recipient phone</label>
						<div class="input-group">
							<div class="input-group-addon">
								<img src="/images/phone-icon.png" width="32" height="32" alt="phone-icon">
							</div>
							<input type="text" id="orders-phone_to" class="form-control" name="Orders[phone_to]" maxlength="255" aria-required="true">
						</div>						
						
						<div class="help-block checkPhone"></div>
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="col-md-5 add-my-place-block">
						<?= $form->field($model, 'address_to')->textInput(['maxlength' => true]) ?>
					</div>
					
					<div class="col-md-2">
						<?= $form->field($model, 'to_app')->textInput(['maxlength' => true]) ?>
					</div>
					<div class="col-md-5 form-group find-btn-div">
						<label class="control-label button-location-order">&nbsp</label>
						<button type="button" class="check-me btn btn-success btn-block">Check Location</button>
					</div>
				</div>
				
    		<div id="mapto"></div>
    	</div>
    	
    </div>
    
    
		<div id = "other">
		
		</div>
		
		
		
		<!--div class="col-md-12">
	    <div class="col-md-12 use-my-place-block places-blocks-to-show"<?= $model->isNewRecord ? ' style="display:none;"' : ''; ?>>
	    	<?= $form->field($model, 'from_id')->dropdownList(
	    		Coordinates::find()->select(['name', 'id'])->where(['user_id' => (int)Yii::$app->user->id])->indexBy('id')->column(),
	    		['prompt'=>Yii::t('app','My places')]) ?>
			</div>
			<div class="col-md-12 use-my-place-block places-blocks-to-show"<?= $model->isNewRecord ? ' style="display:none;"' : ''; ?>>
	    	<?= $form->field($model, 'to_id')->dropdownList(
	    		Coordinates::find()->select(['name', 'id'])->where(['user_id' => (int)Yii::$app->user->id])->indexBy('id')->column(),
	    		['prompt'=>Yii::t('app','My places')]) ?>
			</div>
		</div-->
		
		
		<div class="col-md-10">
			<div class="col-md-4 form-group">
				<label class="control-label" style="width:100%">Weight</label>
			</div>
			<div class="col-md-3"><?= $form->field($model, 'weight')->textInput()->label(false) ?></div>
		</div>
		
		<div class="col-md-10">
			<div class="col-md-4 form-group">
				<label class="control-label button-location-order" style="width:100%">&nbsp</label>
				<label class="control-label" style="width:100%">Dimension of departure</label>
			</div>
			<div class="col-md-2"><?= $form->field($model, 'height')->textInput() ?></div>
			<div class="col-md-2"><?= $form->field($model, 'width')->textInput() ?></div>
			<div class="col-md-2"><?= $form->field($model, 'length')->textInput() ?></div>
			<div class="col-md-2">
				<label class="control-label button-location-order" style="width:100%">&nbsp</label>
				<span class="pull-left">cm</span>
			</div>
		</div>
		<div class="col-md-9">
			<hr>
		</div>
		<div class="col-md-10">
			<div class="col-md-4 form-group">
				<label class="control-label button-location-order" style="width:100%">Number of seats</label>
			</div>
			<div class="col-md-3"><?= $form->field($model, 'seats')->textInput()->label(false) ?></div>
		</div>
		<div class="col-md-10">
			<div class="col-md-4 form-group">
				<label class="control-label button-location-order" style="width:100%">Announcement the cost</label>
			</div>
			<div class="col-md-3"><?= $form->field($model, 'ancost')->textInput()->label(false) ?></div>
		</div>
		<div class="col-md-9">
			<hr>
		</div>
		
		<div class="col-md-10">
			<div class="col-md-4 form-group">
				<label class="control-label button-location-order" style="width:100%">Type of return shipping</label>
			</div>
			<div class="col-md-3"><?= $form->field($model, 'type_id')->dropdownList([1=>'Sender','Recipient'])->label(false) ?></div>
		</div>
		
		<div class="col-md-10">
			<div class="col-md-4 form-group">
				<label class="control-label button-location-order" style="width:100%">Amount</label>
			</div>
			<div class="col-md-3"><?= $form->field($model, 'price')->textInput()->label(false) ?></div>			
		</div>

    <div class="form-group pull-right submit-delivery">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
var schools = [];
</script>