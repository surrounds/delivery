<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = '35000'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view container">

    

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Tracking'), Url::to(['orders/track', 'id' => '350000'.$model->id]), ['class' => 'btn btn-info']) ?>
        <?= Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php
    	if($model->status == "done") $classSpan="btn-default";
    	elseif($model->status == "in-progress") $classSpan="btn-info";
    	elseif($model->status == "pickup") $classSpan="btn-warning";    	
    	else $classSpan="btn-success";
    ?>
    <span class="pull-left btn <?=$classSpan?>" style="margin-top: 5px;"><?=$model->status?></span>
    <h3 class="pull-left">Delivery order <?=$model->consignment?></h3>
    
    <!--div class="col-md-12">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="col-md-12">
						<div class="col-md-5">Sender name</div>
						<div class="col-md-7"><?=$model->clientfio['first_name'].' '.$model->clientfio['last_name']?></div>
					</div>
					<div class="col-md-12">
						<div class="col-md-5">Sender address</div>
						<div class="col-md-7"><?=$model->address_from?></div>
					</div>
					<div class="col-md-12">
						<div class="col-md-5">Sender phone</div>
						<div class="col-md-7"><?=$model->phone?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12">
						<div class="col-md-5">Recipient</div>
						<div class="col-md-7"><?=$model->vendorfio['first_name'].' '.$model->vendorfio['last_name']?></div>
					</div>
					<div class="col-md-12">
						<div class="col-md-5">Recipient address</div>
						<div class="col-md-7"><?=$model->address_to?></div>
					</div>
					<div class="col-md-12">
						<div class="col-md-5">Recipient phone</div>
						<div class="col-md-7"><?=$model->phone_to?></div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2">Price</div>
				<div class="col-md-2">Weight</div>
				<div class="col-md-2">Length</div>
				<div class="col-md-2">Width</div>
				<div class="col-md-2">Height</div>
				<div class="col-md-2">Cost</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"><?=$model->price?></div>
				<div class="col-md-2"><?=$model->weight?></div>
				<div class="col-md-2"><?=$model->length?></div>
				<div class="col-md-2"><?=$model->width?></div>
				<div class="col-md-2"><?=$model->height?></div>
				<div class="col-md-2"><?=$model->cost?></div>
			</div>
		</div-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'client_id',
			[
				'format' => 'raw',
				'attribute' => 'client_id',
				'value' => function($data){return $data->clientfio['first_name'].' '.$data->clientfio['last_name'] ;}
			],
			[
				'format' => 'raw',
				'attribute' => 'recipient_id',
				'value' => function($data){return $data->recipientfio['first_name'].' '.$data->recipientfio['last_name'] ;}
			],
			[
				'format' => 'raw',
				'attribute' => 'vendor_id',
				'value' => function($data){return $data->vendorfio['first_name'].' '.$data->vendorfio['last_name'] ;}
			],
			'consignment',
            //'from_id',
            //'to_id',
			'address_from',
			'address_to',
            'phone',
            'comment',
            'price',
            'weight',
            'length',
            'width',
            'height',
            'cost',
            [
            	'format' => 'raw',
            	'attribute' => 'type_id',
            	'value' => function($data){return $data->type_id == 1 ? 'Sender' : 'Recipient' ;}
            ]
        ],
    ]) ?>

</div>
