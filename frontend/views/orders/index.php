<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Deliveries');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="orders-index-section">
<div class="orders-index container">

    <?= Html::a(Yii::t('app', 'New Delivery'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
    <h3 class="text-left"><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    
    <?php /*foreach($model as $k => $mod):?>
				
				<div class="col-md-12">
					<div class="col-md-4"><?=$mod->addressfrom['name']?></div>
					<div class="col-md-4"><?=$mod->addressto['name']?></div>
					<div class="col-md-2"><?=date('d-m-Y',$mod->created)?></div>
					<div class="col-md-2"><?=date('d-m-Y',$mod->completed)?></div>
				</div>
		
		<?php endforeach;*/ ?>
    <br>
		<?php  Pjax::begin(); echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
				'layout'=>"\n{items}\n{pager}",
				'tableOptions' => ['class'=>'table table-bordered table-striped table-hover'],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'client_id',
			[
				'format' => 'raw',
				'attribute' => 'from_id',
				'value' => function($data){return explode(',',$data->addressfrom['name'])[0];}
			],
			[
				'format' => 'raw',
				'attribute' => 'to_id',
				'value' => function($data){return explode(',',$data->addressto['name'])[0];}
			],
			[
				'format' => 'raw',
				'label' => 'Data',
				'contentOptions' => ['class' => 'order-inder-data'],
				'attribute' => 'created',
				'value' => function($data){return date('d-m-Y',$data->created);}
			],
            //'vendor_id',
            //'from_id',
            // 'phone',
            // 'city_id',
            // 'adress',
            // 'house',
            // 'comment',
            // 'price',
            // 'weight',
            // 'length',
            // 'width',
            // 'height',
            // 'cost',
            // 'type_id',

            [
            		'class' => 'frontend\components\ButtonUpdateDelete', 
            		//'class' => 'yii\grid\ActionColumn',
            		'template'=>'{view} {order_type}',
            		'contentOptions' => ['style' => 'width: 200px;','class' => 'text-left'],
            		'header'=>'Actions', 
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>
</section>
