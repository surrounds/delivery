<header class="page-header page-header--main">
        <!-- RD Navbar -->
        <div class="rd-navbar-wrap">
            <nav class="rd-navbar" data-md-device-layout="rd-navbar-fixed" data-rd-navbar-lg="rd-navbar-static" data-lg-device-layout="rd-navbar-static">
                <div class="rd-navbar-inner">
                    <!-- RD Navbar Panel -->
                    <div class="rd-navbar-panel">

                        <!-- RD Navbar Toggle -->
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar"><span></span></button>
                        <!-- END RD Navbar Toggle -->

                        <!-- RD Navbar Brand -->
                        <div class="rd-navbar-brand">
                            <a href="/" class="brand-name">
                                <img src="/images/logo.jpg" alt="" width="309" height="84">
                            </a>
                        </div>
                        <!-- END RD Navbar Brand -->
                    </div>
                    <!-- END RD Navbar Panel -->

                    <div class="rd-navbar-nav-wrap">
                        <!-- RD Navbar Nav -->
                        <ul class="rd-navbar-nav">
                            <!--li<?php if(Yii::$app->request->url == '/') echo ' class="active"'; ?>>
                                <a href="/">Home</a>
                                
                                <ul class="rd-navbar-megamenu">
                                    <li>
                                        <img src="/images/page-1_img03.jpg" alt="" width="232" height="167">
                                        <ul class="marked-list">
                                            <li><a href="#">Shipping</a></li>
                                            <li><a href="#">Tracking</a></li>
                                            <li><a href="#">Customs Services</a></li>
                                            <li><a href="#">Export Services</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <img src="/images/page-1_img04.jpg" alt="" width="232" height="167">
                                        <ul class="marked-list">
                                            <li><a href="#">Import Services</a></li>
                                            <li><a href="#">Domestic Services</a></li>
                                            <li><a href="#">Optional Services</a></li>
                                            <li><a href="#">Resource Center</a></li>
                                        </ul>
                                    </li>
                                </ul>
                                
                            </li-->
                            <li<?php if(Yii::$app->request->url == '/orders') echo ' class="active"'; ?>>
                                <a href="#">Deliveries</a>
                                <ul class="rd-navbar-dropdown">
                                	<?php if(Yii::$app->user->id):?><li><a href="/orders">My deliveries</a></li><?php endif;?>
                                  <li><a href="/orders/create/">New delivery</a></li>
                                  <li><a href="/orders/calculator/">Calculator</a></li>
                                </ul>
                            </li>
                            <li<?php if(Yii::$app->request->url == '/about') echo ' class="active"'; ?>>
                                <a href="/about">About</a>

                                <!-- RD Navbar Dropdown -->
                                <ul class="rd-navbar-dropdown">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">For Private Customers</a>
                                        <ul class="rd-navbar-dropdown">
                                            <li><a href="#">Shipping</a></li>
                                            <li><a href="#">Delivery</a></li>
                                            <li><a href="#">Payment</a></li>
                                            <li><a href="#">Additional Services</a></li>
                                            <li><a href="#">Documents</a></li>
                                            <li><a href="#">Rates</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">For Business Customers</a></li>
                                    <li><a href="#">International delivery</a></li>
                                    <li><a href="#">Depots</a></li>
                                    <li><a href="#">Expand your Opportunities</a></li>
                                </ul>
                                <!-- END RD Navbar Dropdown -->

                            </li>
                            <li<?php if(Yii::$app->request->url == '/services') echo ' class="active"'; ?>>
                                <a href="/services">Services</a>
                            </li>
                            <!--li<?php if(Yii::$app->request->url == '/news') echo ' class="active"'; ?>>
                                <a href="/news">news</a>
                            </li-->
                            <li<?php if(Yii::$app->request->url == '/contacts') echo ' class="active"'; ?>>
                                <a href="/contacts">Contacts</a>
                            </li>
							<li>
								<?php if(Yii::$app->user->isGuest): ?>
									<a href="/login">Signin/Signup</a>
								<?php else: ?>
									<a href="/profile">Profile</a>
									<ul class="rd-navbar-dropdown">
										<?php if(common\models\Partners::findOne(['user_id' => Yii::$app->user->id])): ?>
										<li><a href="/partners">Partner</a></li>
										<?php endif; ?>
										<li><a href="/logout">Logout</a></li>
									</ul>
								<?php endif; ?>
							</li>
                        </ul>
                        <!-- END RD Navbar Nav -->
                    </div>

                    <!-- RD Navbar Panel -->
                    <div class="rd-navbar-panel">
                        <!-- Contact info -->
                        <address class="contact-info">
                            <a href="callto:#"><span class="icon icon-xs icon-primary fa-phone"></span><?=Yii::$app->settings->phone?></a>
                        </address>
                        <!-- END Contact info -->
                    </div>
                    <!-- END RD Navbar Panel -->
                </div>
            </nav>
        </div>
        <!-- END RD Navbar -->

        <!-- Swiper -->
        <?php if(Yii::$app->controller->id == "site" && Yii::$app->controller->action->id == 'index'): ?>
        <div class="swiper-container swiper-slider" data-height="100vh" data-min-height="350px">
            <div class="swiper-wrapper text-center">
                <div class="swiper-slide" data-slide-title="International delivery" data-slide-bg="/images/slide-1.jpg">
                    <div class="swiper-slide-caption">
                        <div class="container">
                            <h1><span class="text-primary">International delivery</span>
                                <span class="heading-3">4 days - 150 countries</span></h1>
                            <p class="inset-1">Delivery Co. provides international express delivery of documents, parcels and freight to
                                more than 150 countries around the world within 4 to 7 business days.</p>
                            <a href="#" class="btn btn-sm btn-primary">
                                <i class="icon fa-angle-right"></i>
				                <span>read more</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide" data-slide-title="We are proud to be always on demand"
                     data-slide-bg="/images/slide-2.jpg">
                    <div class="swiper-slide-caption">
                        <div class="container">
                            <h1><span class="text-primary">We are proud to</span>
                                <span class="heading-3">be always on demand</span></h1>
                            <p class="inset-1">Delivery Co. provides international express delivery of documents, parcels and freight to more than 150 countries around the world with a choice of delivery periods of 4 or 7 days.</p>
                            <a href="#" class="btn btn-sm btn-primary">
                                <i class="icon fa-angle-right"></i>
                                <span>read more</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide" data-slide-title="Providing the highest" data-slide-bg="/images/slide-3.jpg">
                    <div class="swiper-slide-caption">
                        <div class="container">
                            <h1><span class="text-primary">Providing the highest</span>
                                <span class="heading-3">quality service 24 hours a day</span></h1>
                            <p class="inset-1">Delivery Co. provides international express delivery of documents, parcels and freight to more than 150 countries around the world with a choice of delivery periods of 4 or 7 days.</p>
                            <a href="#" class="btn btn-sm btn-primary">
                                <i class="icon fa-angle-right"></i>
                                <span>read more</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Swiper Navigation -->
            <div class="swiper-button swiper-button-prev">
                <span class="swiper-button__arrow fa-angle-left"></span>

                <div class="preview">
                    <p class="title"></p>
                </div>
            </div>
            <div class="swiper-button swiper-button-next">
                <span class="swiper-button__arrow fa-angle-right"></span>

                <div class="preview">
                    <p class="title"></p>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <!-- END Swiper -->
    </header>