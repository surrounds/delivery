<?php

namespace frontend\components;

use yii\grid\ActionColumn;
use Yii;
use yii\helpers\Html;
use rmrevin\yii\fontawesome\FA;

class ButtonUpdateDelete extends ActionColumn
{

    public $template = '{view} {update} {delete}';
    public $updateAction, $deleteAction;

    protected function initDefaultButtons()
    {
        if (!isset($this->buttons['view'])) {
            $this->buttons['view'] = function ($url, $model) {
                return Html::a(FA::i('eye'), $url, [
                            'title' => Yii::t('app', 'View'),
                            'data-pjax' => '0',
                            'id' => (isset($model->id) ? 'view-button-id-' . $model->id : 0),
                            'class' => 'view-button'
                ]);
            };
        }
        if (!isset($this->buttons['order_type'])) {
            $this->buttons['order_type'] = function ($url, $model) {
            	if($model->status == 'new'){
            		$classBtn = 'btn-success';
            		$btnName = $model->status;
            	}elseif($model->status == 'in-progress'){
            		$classBtn = 'btn-info';
            		$btnName = 'accept';
            	}elseif($model->status == 'pickup'){
            		$classBtn = 'btn-info';
            		$btnName = $model->status;
            	}elseif($model->status == 'done'){
            		$classBtn = 'btn-danger';
            		$btnName = $model->status;
            	}else{
            		$classBtn = 'btn-success';
            		$btnName = $model->status;            		
            	}
            		
                return Html::button(strtoupper($btnName), [
                            'title' => strtoupper($btnName),
                            'data-pjax' => '0',
                            'id' => (isset($model->id) ? 'order_type-button-id-' . $model->id : 0),
                            'class' => 'order_type-button btn '.$classBtn
                ]);
            };
        }
        if (!isset($this->buttons['partner_view'])) {
            $this->buttons['partner_view'] = function ($url, $model) {
                return Html::a(FA::i('eye'), '/partners/orders/view?id='.$model->id, [
                            'title' => Yii::t('app', 'View'),
                            'data-pjax' => '0',
                            'id' => (isset($model->id) ? 'view-button-id-' . $model->id : 0),
                            'class' => 'view-button'
                ]);
            };
        }
        if (!isset($this->buttons['done_order'])) {
            $this->buttons['done_order'] = function ($url, $model) {
                return Html::a(FA::i('truck'), $url, [
						'title' => Yii::t('app', 'Done'),
						'data-confirm' => Yii::t('app', 'Is order finished?'),
						'data-method' => 'post',
						'data-pjax' => '0',
						'class' => 'done_order-button'
                ]);
            };
        }
        if (!isset($this->buttons['update'])) {
            $this->buttons['update'] = function ($url, $model) {
            	
              return Html::a(FA::i('pencil'), $url, [
                          'title' => Yii::t('app', 'Edit Delivery'),
                          'data-pjax' => '0',
                          'id' => (isset($model->id) ? 'edit-button-id-' . $model->id : 0),
                          'class' => 'edit-button'
              ]);
            };
        }
        if (!isset($this->buttons['accept'])) {
            $this->buttons['accept'] = function ($url, $model) {
            //$url = "";
            	
				return Html::a(FA::i('check'), $url, [
				          'title' => Yii::t('app', 'Accept Delivery'),
				          'data-pjax' => '0',
				          'id' => (isset($model->id) ? 'accepter-button-id-' . $model->id : 0),
				          'data-id' => (isset($model->id) ? $model->id : 0),
				          'class' => 'accepter-button'
				]);
            };
        }
//    echo "<pre>";var_dump(User::findIdentity(Yii::$app->user->id)->status);exit;
        if (!isset($this->buttons['delete'])) {
            $this->buttons['delete'] = function ($url, $model) {
              return Html::a(FA::i('trash-o'), $url, [
                          'title' => Yii::t('app', 'Delete'),
                          'data-confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                          'data-method' => 'post',
                          'data-pjax' => '0',
                          'class' => 'delete-button'
              ]);
            };
        }
    }

}
