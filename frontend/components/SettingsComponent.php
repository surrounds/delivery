<?php
namespace frontend\components;

use Yii;
use common\models\Settings;

/**
 * Extended yii\web\User
 *
 * This allows us to do "Yii::$app->user->something" by adding getters
 * like "public function getSomething()"
 *
 * So we can use variables and functions directly in `Yii::$app->user`
 */
class SettingsComponent
{
	public function __construct()
	{
		$settings = Settings::find()->all();
		
		foreach ($settings as $setting)
		{
			$this->{$setting->name} = $setting->value;
		}
	}
    public function getUsername()
    {
        return \Yii::$app->user->identity->username;
    }
    
    public function attr($attr)
    {
    		if(isset(\Yii::$app->user->identity->$attr))
    			$retAttr = \Yii::$app->user->identity->$attr;
    		elseif($retAttr = Userprofile::find()->where(['user_id' => \Yii::$app->user->id])->one()->$attr){}
    		else $retAttr = '';
    		
      	return $retAttr;
    }

    public function getBoss_id()
    {
        return \Yii::$app->user->identity->boss_id;
    }

    public function getPriv()
    {
    	if(!\Yii::$app->user->isGuest)
        return \Yii::$app->user->identity->priv;
      else
      	return '';
    }
}