<?php

namespace frontend\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    		'css/style.css',
    		'//fonts.googleapis.com/css?family=Maven+Pro:400,500,700,900',
			'css/site.css'
    ];
    public $js = [
    		'js/site.js',
    		'js/script.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public function init()
		{
		    parent::init();
		    if(stripos(Yii::$app->request->url,'/contacts') !== false)
		    {
		    	$this->js[] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0&language=en';
		    }
		    
		    if(stripos(Yii::$app->request->url,'/orders/create') !== false || stripos(Yii::$app->request->url,'/orders/update') !== false || stripos(Yii::$app->request->url,'/profile') !== false)
		    {
		    	$this->js[] = 'js/map.script.js?v='.date("mdH");
		    	$this->js[] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0&callback=initMap&language=en';
			}	
			if(stripos(Yii::$app->request->url,'/profile') == false){
				$this->js[] = '/js/profile.js?v='.date("mdH");
				$this->js[] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0&callback=initMap&language=en';
			} 
			if(stripos(Yii::$app->request->url,'partners') !== false && stripos(Yii::$app->request->url,'partnership') == false){
				$this->js[] = '/js/partners.js?v='.date("mdH");
				$this->css[] = '/css/partners.css';
				if(stripos(Yii::$app->request->url,'partners/orders/index') == false && stripos(Yii::$app->request->url,'partners/') == false && stripos(Yii::$app->request->url,'partners/orders/') == false)
					$this->js[] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0&callback=initMap&language=en';
				if(stripos(Yii::$app->request->url,'/partners/orders/view') !== false || stripos(Yii::$app->request->url,'/partners/profile') !== false)
					$this->js[] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0&callback=initMap&language=en';
			}	
			if(stripos(Yii::$app->request->url,'tracking') !== false || stripos(Yii::$app->request->url,'track_search') !== false){
				$this->js[] = '/js/tracking.js?v='.date("mdH");
				$this->js[] = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0&callback=initMap&language=en';
			}
				/*
		    $this->js[] = [		        
		        'async' => true,
		        'defer' => true,
		    ];*/
		}
    
    
}
