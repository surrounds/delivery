
  	$(document).on('click','.navigate-button',function(event) {
  	  event.preventDefault();
  	  $('.navigate-button').removeClass('active');
  	  $(this).addClass('active');
  	  if($(this).attr('data-name') == 'client'){
  	  	$('.user-profile').show();
  	  	$('.partner-profile').hide();
  	  }
  	  if($(this).attr('data-name') == 'partner'){
  	  	$('.partner-profile').show();
  	  	$('.user-profile').hide();
  	  	google.maps.event.trigger(map, 'resize');
  	  }  	  
  	});