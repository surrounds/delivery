<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Orders'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
			[
				'format' => 'raw',
				'attribute' => 'client_id',
				'value' => function($data){return $data->clientfio['first_name'].' '.$data->clientfio['last_name'];}
			],
			[
				'format' => 'raw',
				'attribute' => 'vendor_id',
				'value' => function($data){return $data->recipientfio['first_name'].' '.$data->recipientfio['last_name'];}
			],
			[
				'format' => 'raw',
				'attribute' => 'from_id',
				'value' => function($data){return explode(',',$data->addressfrom['name'])[0];}
			],
			[
				'format' => 'raw',
				'attribute' => 'to_id',
				'value' => function($data){return explode(',',$data->addressto['name'])[0];}
			],
            // 'phone',
            // 'adress',
            // 'house',
            // 'comment',
             'price',
            // 'weight',
            // 'length',
            // 'width',
            // 'height',
            // 'cost',
            // 'type_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
